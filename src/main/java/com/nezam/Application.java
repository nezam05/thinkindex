package com.nezam;

import com.nezam.entity.Person;
import com.nezam.entity.PersonCategory;
import com.nezam.repo.BusinessRepo;
import com.nezam.repo.PersonCategoryRepo;
import com.nezam.repo.PersonRepo;
import nz.net.ultraq.thymeleaf.LayoutDialect;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import javax.transaction.Transactional;
import java.util.List;

@SpringBootApplication
public class Application implements CommandLineRunner {
//    @Autowired
//    private PersonRepo personRepo;
//    @Autowired
//    private BusinessRepo businessRepo;
//    @Autowired
//    private PersonCategoryRepo personCategoryRepo;

    @Bean
    public LayoutDialect layoutDialect() {
        return new LayoutDialect();
    }

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }

    @Override
    @Transactional
    public void run(String... strings) throws Exception {
//        PersonCategory cat1=new PersonCategory("Entrepreneur");
//        PersonCategory cat2 = new PersonCategory("Investor");
//        personCategoryRepo.save(cat1);
//        personCategoryRepo.save(cat2);
//        Person p1=new Person("Nezami","Lalbag, Dhaka","http://nezamuddin.com");
////        Business b1=new Business("Future Startup","A startup that faciliates others",
////                "http://google.com","Mohammadpur, Dhaka");
////        Business b2=new Business("ThinkPool","A startup that faciliates others",
////                "http://google.com","Mohammadpur, Dhaka");
//        Affiliation af1=new Affiliation();
//        Affiliation af2=new Affiliation();
////        af1.setBusiness(b1);
////        af2.setBusiness(b2);
//        af1.setPerson(p1);
////        af1.setRole("CoFounder");
////        af2.setPerson(p1);
////        af2.setRole("Founder");
//        p1.getAffiliation().add(af1);
////        p1.getAffiliation().add(af2);
//
//        //delete
//        Person p1 = personRepo.getOne(1);
////        Business b1=businessRepo.getOne(2);
//        Investment inv1=new Investment(20000,"Seed round");
////        inv1.setPerson(p1);d
////        inv1.setBusiness(b1);
//
////        Iterator<Affiliation> itr = p1.getAffiliation().iterator();
////        //show role + business name
////        for (Affiliation item : p1.getAffiliation()) {
////            System.out.println("role: " + item.getRole() + " " + item.getBusiness().getName());
////        }
////        System.out.println("here we are >>> " + p1.getAffiliation().size());
//
//        p1.getInvestmentList().add(inv1);
////        b1.getInvestmentList().add(inv1);
//
//        //category test

//        BusinessCategory cat1=new BusinessCategory("Entrepreneur");
//        BusinessCategory cat2=new BusinessCategory("Investor");
////        BusinessCategory cat3=new BusinessCategory("Food Processor");
////        BusinessCategory cat4=new BusinessCategory("Digital Service Provider");
//        p1.setCategories(new ArrayList<BusinessCategory>(){{add(cat1); add(cat2);}});
//        Business b1=businessRepo.getOne(2);
//        inv1.setBusiness(b1);
//        af1.setBusiness(b1);
//        personRepo.save(p1);
////
//        b1.setCategories(new ArrayList<BusinessCategory>(){{add(cat3);add(cat4);}});
//        businessRepo.save(b1);

        //find by person name
//        List<Person> list = personRepo.findByNameContainingIgnoreCase("");
//        for (Person l : list) {
//            System.out.println(l.getName());
//        }
    }

}
