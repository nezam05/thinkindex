package com.nezam.controller;

import com.nezam.entity.Business;
import com.nezam.entity.Person;
import com.nezam.helpers.StorageService;
import com.nezam.repo.BusinessCategoryRepo;
import com.nezam.repo.BusinessRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

@Controller
public class BusinessController {

    @Autowired
    private StorageService storageService;
    @Autowired
    private BusinessRepo businessRepo;
    @Autowired
    private BusinessCategoryRepo businessCategoryRepo;

    @PostMapping(value = "/addBusiness")
    public String createBusiness(@ModelAttribute Business business, @RequestParam("file") MultipartFile file) {
        storageService.store(file);
        String fileDownloadUri = ServletUriComponentsBuilder.fromCurrentContextPath()
                .path("/files/")
                .path(file.getOriginalFilename())
                .toUriString();
        business.setLogoUrl(fileDownloadUri);
        Business business1 = businessRepo.save(business);
        return "redirect:/business/" + business1.getId();
    }

    @GetMapping("/add-business")
    public String BusinessPage(Model model) {
        model.addAttribute("businessCategories", businessCategoryRepo.findAll());
        model.addAttribute("business", new Business());
        return "add-business";
    }

    @GetMapping(value = "/business/{id}")
    public String showSingleBusiness(Model model, @PathVariable int id) {
        model.addAttribute("business", businessRepo.getOne(id));
        return "single-business";
    }

    @GetMapping(value = "/business/{id}/edit")
    public String editSinglePerson(Model model, @PathVariable int id) {
        Business business = businessRepo.getOne(id);
        model.addAttribute("businessCategories", businessCategoryRepo.findAll());
        model.addAttribute("business", business);
        return "edit-business";
    }

    @PostMapping(value = "/saveEditBusiness")
    public String saveEditBusiness(@ModelAttribute Business business, @RequestParam("file") MultipartFile file) {
        if (!file.isEmpty()) {
            storageService.store(file);
            String fileDownloadUri = ServletUriComponentsBuilder.fromCurrentContextPath()
                    .path("/files/")
                    .path(file.getOriginalFilename())
                    .toUriString();
            business.setLogoUrl(fileDownloadUri);
        } else {
            business.setLogoUrl(businessRepo.getOne(business.getId()).getLogoUrl());
        }
        Business singleBusiness = businessRepo.save(business);
        return "redirect:/business/" + singleBusiness.getId();
    }

    @GetMapping(value = "/businesses")
    public String getAllProfiles(Model model) {
        model.addAttribute("businesses", businessRepo.findAll());
        return "all-businesses";
    }
}
