package com.nezam.controller;

import com.nezam.entity.Affiliation;
import com.nezam.entity.Business;
import com.nezam.entity.Investment;
import com.nezam.entity.Person;
import com.nezam.helpers.SearchService;
import com.nezam.helpers.StorageService;
import com.nezam.repo.BusinessCategoryRepo;
import com.nezam.repo.BusinessRepo;
import com.nezam.repo.PersonCategoryRepo;
import com.nezam.repo.PersonRepo;
import com.sun.org.apache.xpath.internal.operations.Mod;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.util.List;

@Controller
public class PersonController {

    @Autowired
    private StorageService storageService;

    @Autowired
    private PersonRepo personRepo;

    @Autowired
    private BusinessRepo businessRepo;

    @Autowired
    private PersonCategoryRepo personCategoryRepo;

    @RequestMapping(value = "/")
    public String home(Model model) {
        model.addAttribute("search", new SearchService());
        return "home";
    }

    @GetMapping("/add-person")
    public String personPage(Model model) {
        model.addAttribute("personCategories", personCategoryRepo.findAll());
        model.addAttribute("person", new Person());
        return "add-person";
    }

    @PostMapping(value = "/addPerson")
    public String createPerson(@ModelAttribute Person person, @RequestParam("file") MultipartFile file) {
        if (!file.isEmpty()) {
            storageService.store(file);
            String fileDownloadUri = ServletUriComponentsBuilder.fromCurrentContextPath()
                    .path("/files/")
                    .path(file.getOriginalFilename())
                    .toUriString();
            person.setImageUrl(fileDownloadUri);
        }
        Person singlePerson = personRepo.save(person);
//        model.addAttribute("id", singlePerson.getId());
        return "redirect:/person/" + singlePerson.getId();
    }

    @GetMapping(value = "/person/{id}")
    public String showSinglePerson(Model model, @PathVariable int id) {
        model.addAttribute("person", personRepo.getOne(id));
        return "single-person";
    }

    @GetMapping(value = "/person/{id}/edit")
    public String editSinglePerson(Model model, @PathVariable int id) {
        Person person = personRepo.getOne(id);
        model.addAttribute("personCategories", personCategoryRepo.findAll());
        model.addAttribute("person", person);
        return "edit-person";
    }

    @PostMapping(value = "/saveEditPerson")
    public String saveEditPerson(@ModelAttribute Person person, @RequestParam("file") MultipartFile file) {
        if (!file.isEmpty()) {
            storageService.store(file);
            String fileDownloadUri = ServletUriComponentsBuilder.fromCurrentContextPath()
                    .path("/files/")
                    .path(file.getOriginalFilename())
                    .toUriString();
            person.setImageUrl(fileDownloadUri);
        } else {
            person.setImageUrl(personRepo.getOne(person.getId()).getImageUrl());
        }
        Person singlePerson = personRepo.save(person);
//        model.addAttribute("id", singlePerson.getId());
        return "redirect:/person/" + singlePerson.getId();
    }

    @GetMapping(value = "/person/{id}/add-affiliation")
    public String addAffiliation(Model model, @PathVariable int id) {
        Person person = personRepo.getOne(id);
        model.addAttribute("businessList", businessRepo.findAll());
        model.addAttribute("person", person);
        model.addAttribute("affiliation", new Affiliation());
        return "add-affiliation";
    }

    @PostMapping(value = "/saveAffiliation")
    public String saveAffiliation(@ModelAttribute("person") Person person,
                                  @ModelAttribute("affiliation") Affiliation aff) {
        Person updatePerson = personRepo.getOne(person.getId());
        aff.setPerson(updatePerson);
        updatePerson.getAffiliation().add(0, aff);
        personRepo.save(updatePerson);
        return "redirect:/person/" + person.getId();
    }

    @GetMapping(value = "/person/{id}/add-investment")
    public String addInvestment(Model model, @PathVariable int id) {
        Person person = personRepo.getOne(id);
        model.addAttribute("businessList", businessRepo.findAll());
        model.addAttribute("person", person);
        model.addAttribute("investment", new Investment());
        return "add-investment";
    }

    @PostMapping(value = "/saveInvestment")
    public String saveInvestment(@ModelAttribute("person") Person person,
                                 @ModelAttribute("investment") Investment inv) {
        Person updatePerson = personRepo.getOne(person.getId());
        inv.setPerson(updatePerson);
        updatePerson.getInvestmentList().add(0, inv);
        personRepo.save(updatePerson);
        return "redirect:/person/" + person.getId();
    }

//    @PostMapping(value = "/search")
//    public String executeSearch(@ModelAttribute SearchService search, Model model) {
//        if (search.getSearchType().equals("person")) {
//        personRepo.findByNameContainingIgnoreCase(search.getSearchTerm());
//        } else if (search.getSearchType().equals("business")) {
//            search.getBusinessResult().addAll(businessRepo.findByNameContainingIgnoreCase(search.getSearchTerm()));
//        }
//
//        model.addAttribute("result", search);
//
//        return "redirect:/search/term/" + search.getSearchTerm();
//    }

    @PostMapping(value = "/search")
    public String showResult(@ModelAttribute SearchService search, Model model) {
        if (search.getSearchType().equals("person")) {
            List<Person> searchResult = personRepo.findByNameContainingIgnoreCase(search.getSearchTerm());
            if (!searchResult.isEmpty()) {
                search.getPersonResult().addAll(searchResult);
            } else {
                model.addAttribute("noPersonFound", "No Person matches your query. Please search again.");
            }
            model.addAttribute("result", search);
            return "person-search-result";
        } else {
            List<Business> searchResult = businessRepo.findByNameContainingIgnoreCase(search.getSearchTerm());
            if (!searchResult.isEmpty()) {
                search.getBusinessResult().addAll(searchResult);
            } else {
                model.addAttribute("noBusinessFound", "No Business matches your query. Please search again.");
            }
            model.addAttribute("result", search);
            return "business-search-result";
        }
    }

    @GetMapping(value = "/profiles")
    public String getAllProfiles(Model model) {
        model.addAttribute("persons", personRepo.findAll());
        return "all-profiles";
    }

}
