package com.nezam.entity;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "affiliation_detail")
@IdClass(AffiliationId.class)
public class Affiliation implements Serializable {
    private Person person;
    private Business business;
    private String role;

    @EmbeddedId
    private AffiliationId affiliationId;

    @Id
    @ManyToOne
    @JoinColumn(name = "Personid")
    public Person getPerson() {
        return person;
    }

    public void setPerson(Person person) {
        this.person = person;
    }

    @Id
    @ManyToOne
    @JoinColumn(name = "Businessid")
    public Business getBusiness() {
        return business;
    }

    public void setBusiness(Business business) {
        this.business = business;
    }

    @Column(name = "role")
    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

}
