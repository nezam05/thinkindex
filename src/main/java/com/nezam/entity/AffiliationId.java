package com.nezam.entity;

import javax.persistence.Embeddable;
import java.io.Serializable;
import java.util.Objects;

@Embeddable
public class AffiliationId implements Serializable {
    private Integer person;
    private Integer business;

    public Integer getPerson() {
        return person;
    }

    public void setPerson(Integer person) {
        this.person = person;
    }

    public Integer getBusiness() {
        return business;
    }

    public void setBusiness(Integer business) {
        this.business = business;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof AffiliationId)) return false;
        AffiliationId that = (AffiliationId) o;
        return Objects.equals(getPerson(), that.getPerson()) &&
                Objects.equals(getBusiness(), that.getBusiness());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getPerson(), getBusiness());
    }
}
