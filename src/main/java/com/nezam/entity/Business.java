package com.nezam.entity;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "business")
public class Business implements Serializable {


    private Integer id;
    private String name;
    private String details;
    private String logoUrl;
    private String address;
    private List<Affiliation> affiliationList;
    private List<Investment> investmentList;
    private List<BusinessCategory> categories;

    public Business() {
    }

    public Business(String name, String details, String logoUrl, String address) {
        this.name = name;
        this.details = details;
        this.logoUrl = logoUrl;
        this.address = address;
        affiliationList = new ArrayList<>();
        investmentList = new ArrayList<>();
    }

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Column(name = "busiess_name")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Column(name = "busiess_details")
    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }

    @Column(name = "logo_url")
    public String getLogoUrl() {
        return logoUrl;
    }

    public void setLogoUrl(String logoUrl) {
        this.logoUrl = logoUrl;
    }

    @Column(name = "busiess_address")
    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    @OneToMany(mappedBy = "business", cascade = CascadeType.ALL)
    public List<Affiliation> getAffiliationList() {
        return affiliationList;
    }

    public void setAffiliationList(List<Affiliation> affiliationList) {
        this.affiliationList = affiliationList;
    }

    @OneToMany(mappedBy = "business", cascade = CascadeType.ALL)
    public List<Investment> getInvestmentList() {
        return investmentList;
    }

    public void setInvestmentList(List<Investment> investmentList) {
        this.investmentList = investmentList;
    }

    @ManyToMany(cascade = {CascadeType.PERSIST, CascadeType.MERGE})
    @JoinTable(name = "business_bcategory", joinColumns = @JoinColumn(name = "business_id",
            referencedColumnName = "id"), inverseJoinColumns = @JoinColumn(name = "bcategory_id", referencedColumnName = "id"))
    public List<BusinessCategory> getCategories() {
        return categories;
    }

    public void setCategories(List<BusinessCategory> categories) {
        this.categories = categories;
    }
}
