package com.nezam.entity;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "bcategory")
public class BusinessCategory implements Serializable {

    private int id;
    private String categoryName;

    public BusinessCategory() {
    }

    public BusinessCategory(String categoryName) {
        this.categoryName = categoryName;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Column(name = "bcategory_name", nullable = false)
    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

}
