package com.nezam.entity;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "investment")
@IdClass(InvestmentId.class)
public class Investment implements Serializable {

    private int investmentAmount;
    private String investmentRound;
    private Person person;
    private Business business;

    @EmbeddedId
    private InvestmentId investmentId;

    public Investment(int investmentAmount, String investmentRound) {
        this.investmentAmount = investmentAmount;
        this.investmentRound = investmentRound;
    }

    public Investment() {
    }

    @Column(name = "investment_amount", nullable = false)
    public int getInvestmentAmount() {
        return investmentAmount;
    }

    public void setInvestmentAmount(int investmentAmount) {
        this.investmentAmount = investmentAmount;
    }

    @Column(name = "investment_round")
    public String getInvestmentRound() {
        return investmentRound;
    }

    public void setInvestmentRound(String investmentRound) {
        this.investmentRound = investmentRound;
    }

    @Id
    @ManyToOne
    @JoinColumn(name = "Personid", referencedColumnName = "id")
    public Person getPerson() {
        return person;
    }

    public void setPerson(Person person) {
        this.person = person;
    }

    @Id
    @ManyToOne
    @JoinColumn(name = "Businessid", referencedColumnName = "id")
    public Business getBusiness() {
        return business;
    }

    public void setBusiness(Business business) {
        this.business = business;
    }

}
