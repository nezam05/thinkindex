package com.nezam.entity;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "person")
public class Person implements Serializable {


    private Integer id;
    private String name;
    private String address;
    private String imageUrl;
    private List<Affiliation> affiliation;
    private List<Investment> investmentList;
    private List<PersonCategory> categories;

    public Person() {
    }

    public Person(String name, String address, String imageUrl) {
        this.name = name;
        this.address = address;
        this.imageUrl = imageUrl;
        affiliation = new ArrayList<>();
        investmentList = new ArrayList<>();
        categories=new ArrayList<>();
    }

    public Person(List<PersonCategory> categories) {
        this.categories = categories;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Column(name = "person_name")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Column(name = "person_address")
    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    @Column(name = "image_url")
    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    @OneToMany(mappedBy = "person", cascade = CascadeType.ALL)
    public List<Affiliation> getAffiliation() {
        return affiliation;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    public Integer getId() {
        return id;
    }

    public void setAffiliation(List<Affiliation> affiliation) {
        this.affiliation = affiliation;
    }

    @OneToMany(mappedBy = "person", cascade = CascadeType.ALL)
    public List<Investment> getInvestmentList() {
        return investmentList;
    }

    public void setInvestmentList(List<Investment> investmentList) {
        this.investmentList = investmentList;
    }

    @ManyToMany(cascade = {CascadeType.PERSIST, CascadeType.MERGE})
    @JoinTable(name = "person_pcategory", joinColumns = @JoinColumn(name = "person_id", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(name = "pcategory_id", referencedColumnName = "id"))
    public List<PersonCategory> getCategories() {
        return categories;
    }

    public void setCategories(List<PersonCategory> categories) {
        this.categories = categories;
    }
}
