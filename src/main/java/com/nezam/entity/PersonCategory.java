package com.nezam.entity;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "pcategory")
public class PersonCategory implements Serializable {

    private int id;
    private String categoryName;

    public PersonCategory() {
    }

    public PersonCategory(String categoryName) {
        this.categoryName = categoryName;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Column(name = "pcategory_name", nullable = false)
    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

}
