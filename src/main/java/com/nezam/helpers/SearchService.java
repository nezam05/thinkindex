package com.nezam.helpers;

import com.nezam.entity.Business;
import com.nezam.entity.Person;

import java.util.ArrayList;
import java.util.List;

public class SearchService {
    private String searchTerm;
    private String searchType;
    private List<Person> personResult;
    private List<Business> businessResult;

    public SearchService() {
        this.personResult = new ArrayList<>();
        this.businessResult = new ArrayList<>();
    }

    public String getSearchTerm() {
        return searchTerm;
    }

    public void setSearchTerm(String searchTerm) {
        this.searchTerm = searchTerm;
    }

    public String getSearchType() {
        return searchType;
    }

    public void setSearchType(String searchType) {
        this.searchType = searchType;
    }

    public List<Person> getPersonResult() {
        return personResult;
    }

    public void setPersonResult(List<Person> personResult) {
        this.personResult = personResult;
    }

    public List<Business> getBusinessResult() {
        return businessResult;
    }

    public void setBusinessResult(List<Business> businessResult) {
        this.businessResult = businessResult;
    }
}
