package com.nezam.helpers;

import org.springframework.core.io.Resource;
import org.springframework.web.multipart.MultipartFile;

import java.nio.file.Path;
import java.util.stream.Stream;

public interface StorageService {

    void init();

    void store(MultipartFile file);

    Path load(String filename);

    Resource loadAsResource(String filename);

/*
Disabled due to its dangerous nature of deleting all resources
    void deleteAll();
*/
/* Disabled due to not required in this project
    Stream<Path> loadAll();
    */
}
