package com.nezam.repo;

import com.nezam.entity.Affiliation;
import com.nezam.entity.BusinessCategory;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AffiliationRepo extends JpaRepository<Affiliation, Integer> {

}
