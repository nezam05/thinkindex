package com.nezam.repo;

import com.nezam.entity.BusinessCategory;
import org.springframework.data.jpa.repository.JpaRepository;

public interface BusinessCategoryRepo extends JpaRepository<BusinessCategory, Integer> {

}
