package com.nezam.repo;

import com.nezam.entity.Business;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface BusinessRepo extends JpaRepository<Business, Integer> {
    List<Business> findByNameContainingIgnoreCase(String name);
}
