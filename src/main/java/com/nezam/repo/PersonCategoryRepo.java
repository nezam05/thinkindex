package com.nezam.repo;

import com.nezam.entity.PersonCategory;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PersonCategoryRepo extends JpaRepository<PersonCategory, Integer> {

}
