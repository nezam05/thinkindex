package com.nezam.repo;

import com.nezam.entity.Person;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface PersonRepo extends JpaRepository<Person, Integer> {
    List<Person> findByNameContainingIgnoreCase(String name);

}
