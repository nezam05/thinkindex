package com.nezam.security;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class SecurityController {

    @GetMapping(value = "/user")
    public String viewUserProfile(){
        return "/security/user-profile";
    }

    @GetMapping(value = "/admin")
    public String viewAdminProfile(){
        return "/security/admin-profile";
    }
    @GetMapping(value = "/members")
    public String viewLogin(){
        return "/security/members";
    }
}
